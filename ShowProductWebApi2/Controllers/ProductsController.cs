﻿using ShowProductWebApi2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace ShowProductWebApi2.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[]
        {
            new Product { ProductId = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 },
            new Product { ProductId = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M },
            new Product { ProductId = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M }

        };

        // returning void 204 no content
        public void Post()
        {
        }

        // returning HttpResponseMessage

        //public HttpResponseMessage Get()
        //{
        //    //// with cache-control
        //    //HttpResponseMessage response = Request.CreateErrorResponse(HttpStatusCode.OK, "value");
        //    //response.Content = new StringContent("Hello", Encoding.Unicode);
        //    //response.Headers.CacheControl = new CacheControlHeaderValue()
        //    //{
        //    //    MaxAge = TimeSpan.FromMinutes(20)
        //    //};

        //    //// Get a list of products from a database.
        //    //IEnumerable<Product> products = GetProductsFromDB();

        //    // Write the list to the response body.
        //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, products);
        //    return response;
        //}


        // Returning IHttpActionResult
        /*advantages of using the IHttpActionResult interface:
          *Simplifies unit testing your controllers.
          **Moves common logic for creating HTTP responses into separate classes.
          ***Makes the intent of the controller action clearer, by hiding the low-level details of constructing the response.
          */
        //public IHttpActionResult Get()
        //{
        //    //return new TextResult("Hello This is using IHttpActionResult", Request);

        //    if (products == null)
        //    {
        //        return NotFound(); // Returns a NotFoundResult
        //    }
        //    return Ok(products);  // Returns an OkNegotiatedContentResult

        //}


        // other return types are serialized
        public IEnumerable<Product> Get()
        {
            return products;
        }

        //public IEnumerable<Product> GetAllProducts()
        //{
        //    return products;
        //}

        //public IHttpActionResult GetProduct(int id)
        //{
        //    var product = products.FirstOrDefault(p => p.ProductId == id);
        //    if(product == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(product);
        //}
    }
}
